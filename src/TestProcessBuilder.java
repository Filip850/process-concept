import step.Step;

import java.util.ArrayList;
import java.util.List;

public class TestProcessBuilder<T>{
    private final List<Step<T>> steps = new ArrayList<>();

    public TestProcessBuilder<T> addStep(Step<T> step) {
        steps.add(step);
        return this;
    }

    public Process<T> build(){
        return new Process<>(steps);
    }


}
