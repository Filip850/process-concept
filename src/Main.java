import step.testProcessSteps.TestStep;
import step.testProcessSteps.TestStepTwo;

public class Main {
    public static void main(String[] args) {

        //string can be changed to an object containing Data for ex. InvoiceData that will be provided between steps.
        Process<String> processTest = new TestProcessBuilder<String>()
                .addStep(new TestStep())
                .addStep(new TestStepTwo("Step two data"))
                .build();
        //Step also can have his own data created inside of step extender or globally
        //in abstract Object (All is just constructor playgrounds)

        processTest.executeSteps("Just some random data");
    }
}


