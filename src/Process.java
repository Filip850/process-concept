import step.Step;

import java.util.List;

public class Process<T>{

    private final List<Step<T>> steps;

    public Process(List<Step<T>> steps) {
        this.steps = steps;
    }

    public void executeSteps(T data) {
        steps.forEach(step -> step.execute(data));
    }

    public List<Step<T>> getSteps() {
        return steps;
    }

}
