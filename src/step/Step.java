package step;

import java.util.Date;

public abstract class Step<T> {

    private final Date createdAt;

    protected Step() {
        this.createdAt = new Date();
    }

    public abstract void execute(T data);

    public Date getCreatedAt() {
        return createdAt;
    }

}
