package step.testProcessSteps;

import step.Step;

public class TestStepTwo extends Step<String> {

    private final String dataOfStep;

    public TestStepTwo(String dataOfStep) {
        //Just adding global step Data
        super();

        //Adding local step data
        this.dataOfStep = dataOfStep;
    }

    @Override
    public void execute(String data) {
        printMessage();
    }

    private void printMessage(){
        System.out.println("Test step two has been executed, hovered: " + dataOfStep);
    }
}
