package step.testProcessSteps;

import step.Step;

public class TestStep extends Step<String> {
    public TestStep() {
        super();
    }

    @Override
    public void execute(String data) {
        printMessage(data);
    }

    private void printMessage(String data){
        System.out.println("Test step has been executed, hovered: " + data + " | Created at: " + getCreatedAt());
    }
}
